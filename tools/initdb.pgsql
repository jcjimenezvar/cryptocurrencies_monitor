-- Drops users table
DROP TABLE IF EXISTS users;
-- Drops users_vs_currencies table
DROP TABLE IF EXISTS users_vs_currencies;

-- Creates users table
CREATE TABLE IF NOT EXISTS users (
    id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY
    , user_name varchar(50) NOT NULL
    , user_last_name varchar(50) NOT NULL
    , username varchar(50) NOT NULL
    , user_password varchar(50) NOT NULL
    , favorite_currency varchar(3) NOT NULL
    , CONSTRAINT users_unique_ UNIQUE (username)
);
-- Creates users_vs_currencies table
CREATE TABLE IF NOT EXISTS users_vs_currencies (
    id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY
    , username varchar(50) NOT NULL
    , currency_id varchar(50) NOT NULL
    , currency_symbol varchar(50) NOT NULL
    , currency_price INT NOT NULL
    , currency_name varchar(50) NOT NULL
    , currency_image varchar(250) NOT NULL
    , currency_last_update varchar(100) NOT NULL
);
### Wolox CryptoCurrencies Monitor Node Express project

Este proyecto es desarrollado con NodeJS y Typescript.

### Configuración 

Para iniciar el proyecto es necesario instalar las siguientes dependencias:
- Docker (https://www.docker.com/products/docker-desktop) --> Lo utilizaremos para crear la base de datos
- NodeJs (https://nodejs.org/es/download/)
- Git (https://git-scm.com/downloads)

### Base de datos
Teniendo Docker instalado debemos ejecutar el siguiente comando `docker pull postgres:latest` que se encargara de descargar la ultima imagen de postgres para Docker, posteriormente ejecutamos `docker run -d --name cryptocurrencies-monitor-db -p 5432:5432 -e 'POSTGRES_PASSWORD=admin123' postgres` este para poder crear la imagen.

### Despliegue

Una vez instaladas puedes clonar el repositorio de (https://gitlab.com/jcjimenezvar/cryptocurrencies_monitor.git) utilizando el comando `git clone https://gitlab.com/jcjimenezvar/cryptocurrencies_monitor.git` cuando el repositorio sea clonado, se debe utilzar el siguiente comando en la terminal(CMD) `cd cryptocurrencies_monitor`. Allí se debe ejecutar el comando `npm i` el cual instalara las dependencias necesarias para que el proyecto funcione, una vez la instalación termine se deben ejecutar los siguientes comandos `npm run initdb` que se encargara de inicializar las tablas necesarias para la app y `npm run start`. Cuando se ejecute el servidor veremos que se muestra por consola `server started at http://localhost:8080`. La app tiene configuración de swagger para poder visualizar la documentación de los endpoints se debe ingresar al endpoint `http:localhost:8080/api-docs`.
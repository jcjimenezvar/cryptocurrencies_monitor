import { DataTypes, Model, Sequelize } from "sequelize";
import dotenv from "dotenv";

dotenv.config();
const db = String(process.env.PGDATABASE);
const username = String(process.env.PGUSER);
const password = String(process.env.PGPASSWORD);

const sequelize = new Sequelize(db, username, password, {
  define: {
    underscored: true,
  },
  dialect: "postgres",
  host: String(process.env.PGHOST),
  port: parseInt(String(process.env.PGPORT), 0),
});

export class Users extends Model {
  public userName: string;
  public userLastName: string;
  public username: string;
  public userPassword: string;
  public favoriteCurrency: string;
}

Users.init(
  {
    user_name: {
      allowNull: false,
      type: new DataTypes.STRING(50),
    },
    user_last_name: {
      allowNull: false,
      type: new DataTypes.STRING(50),
    },
    username: {
      allowNull: false,
      type: new DataTypes.STRING(50),
      unique: true,
    },
    user_password: {
      allowNull: false,
      type: new DataTypes.STRING(50),
    },
    favorite_currency: {
      allowNull: false,
      type: new DataTypes.STRING(3),
    },
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
  },
  {
    sequelize,
    tableName: "users",
    timestamps: false,
  }
);

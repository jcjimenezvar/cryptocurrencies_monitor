import { DataTypes, Model, Sequelize } from 'sequelize'
import dotenv from 'dotenv'

dotenv.config()
const db = String(process.env.PGDATABASE)
const username = String(process.env.PGUSER)
const password = String(process.env.PGPASSWORD)

const sequelize = new Sequelize(db, username, password, {
  define: {
    underscored: true,
  },
  dialect: 'postgres',
  host: String(process.env.PGHOST),
  port: parseInt(String(process.env.PGPORT), 0),
})

export class UsersVsCryptocurrencies extends Model {
  public username: string
  public currencyId: string
  public currencySymbol: string
  public currencyPrice: number
  public currencyName: string
  public currencyImage: string
  public currencyLastUpdate: string
}

UsersVsCryptocurrencies.init(
  {
    username: {
      allowNull: false,
      type: new DataTypes.STRING(50),
      unique: true,
    },
    currency_id: {
      allowNull: false,
      type: new DataTypes.STRING(50),
    },
    currency_symbol: {
      allowNull: false,
      type: new DataTypes.STRING(50),
    },
    currency_price: {
      allowNull: false,
      type: new DataTypes.INTEGER(),
    },
    currency_name: {
      allowNull: false,
      type: new DataTypes.STRING(50),
    },
    currency_image: {
      allowNull: false,
      type: new DataTypes.STRING(250),
    },
    currency_last_update: {
      allowNull: false,
      type: new DataTypes.STRING(100),
    },
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
  },
  {
    sequelize,
    tableName: 'users_vs_currencies',
    timestamps: false,
  }
)

export interface SingUpRequest {
  name: string
  lastName: string
  username: string;
  password: string;
  favoriteCurrency: string
}

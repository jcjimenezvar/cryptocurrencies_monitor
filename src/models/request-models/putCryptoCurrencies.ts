export interface PutCryptoCurrenciesRequest {
  currencyId: string
  currencySymbol: string
  currencyPrice: number
  currencyName: string
  currencyImage: string
  currencyLastUpdated: string
}

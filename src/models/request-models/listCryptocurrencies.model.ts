export interface ListCryptoCurrenciesResponse {
  currency: string
  symbol: string
  price: number
  currencyName: string
  currencyImage: string
  lastUpdateDate: string
}

import express from 'express'
import dotenv from 'dotenv'
import morgan from 'morgan'
import * as routes from './routes'
import * as bodyParser from 'body-parser'
import swaggerUiExpress from 'swagger-ui-express'
import apiDocsJson from './api-docs.json'
const app = express()

dotenv.config()
const port = process.env.SERVER_PORT

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(morgan('dev'))

// Configure routes
routes.register(app)

// Configure swagger and development environment variables
if (process.env.NODE_ENV !== 'production') {
  dotenv.config()
  app.use(
    '/api-docs',
    swaggerUiExpress.serve,
    swaggerUiExpress.setup(apiDocsJson)
  )
}

// start the Express server
app.listen(port, () => {
  // tslint:disable:no-console
  console.log(`server started at http://localhost:${port}`)
})

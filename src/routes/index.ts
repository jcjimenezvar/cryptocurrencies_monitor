import * as express from 'express'
import { Login } from '../services/login'
import { SignUp } from '../services/signup'
import {
  login,
  signup,
  getCryptoCurrencies,
  putCryptoCurrencies,
} from './schemas'
import { Response } from '../models/response/Reponse.model'
import { JsonWebToken } from '../commons/jsonWebToken'
import { GetCryptoCurrencies } from '../services/getCryptoCurrencies'
import { PutCryptoCurrencies } from '../services/putCryptoCurrencies'
import { ListCryptoCurrencies } from '../services/listCryptoCurrencies'
export const register = (app: express.Application) => {
  app.post('/login', async (req: any, res) => {
    try {
      const data = req.body
      const validationResult = login.validate(data)
      let httpResponse: Response = {
        data: null,
        errors: [],
      }
      if (validationResult.error) {
        // Si la validación de los datos de la solicitud falla, entonces retorna error 422 / Invalid Request
        httpResponse = {
          data: null,
          errors: ['invalid_request'],
        }
        return res.status(422).send(httpResponse)
      } else {
        const response = await Login.login(req.body)
        if (response.errors && response.errors.length) {
          return res.status(400).send(response)
        }
        return res.status(200).send(response)
      }
    } catch (error) {
      return res.status(500).send(error)
    }
  })
  app.post('/signup', async (req: any, res) => {
    try {
      const data = req.body
      const validationResult = signup.validate(data)
      let httpResponse: Response = {
        data: null,
        errors: [],
      }
      if (validationResult.error) {
        // Si la validación de los datos de la solicitud falla, entonces retorna error 422 / Invalid Request
        httpResponse = {
          data: null,
          errors: ['invalid_request'],
        }
        return res.status(422).send(httpResponse)
      } else {
        const response = await SignUp.signup(req.body)
        if (response.errors.length) {
          return res.status(400).send(response)
        }
        return res.status(200).send(response)
      }
    } catch (error) {
      // tslint:disable:no-console
      console.log(error)
      return res.status(500).send({ error })
    }
  })
  app.get('/get-cryptocurrencies', async (req: any, res) => {
    try {
      const data = req.query
      const validationResult = getCryptoCurrencies.validate(data)
      let httpResponse: Response = {
        data: null,
        errors: [],
      }
      if (validationResult.error) {
        // Si la validación de los datos de la solicitud falla, entonces retorna error 422 / Invalid Request
        httpResponse = {
          data: null,
          errors: ['invalid_request'],
        }
        return res.status(422).send(httpResponse)
      } else {
        const tokenInformation: any = JsonWebToken.verify(data.token)
        if (!tokenInformation.username) {
          httpResponse = {
            data: null,
            errors: ['An error ocurred verifying the token'],
          }
          return res.status(401).send(httpResponse)
        }

        const response: any = await GetCryptoCurrencies.get(
          tokenInformation.username
        )
        if (response.errors && response.errors.length) {
          return res.status(400).send(response)
        }
        return res.status(200).send(response)
      }
    } catch (error) {
      // tslint:disable:no-console
      console.log(error)
      res.status(500).send({ error })
    }
  })
  app.put('/put-user-cryptocurrencies', async (req: any, res) => {
    try {
      const data = { ...req.body, ...req.query }
      // tslint:disable:no-console
      console.log(data)

      const validationResult = putCryptoCurrencies.validate(data)
      let httpResponse: Response = {
        data: null,
        errors: [],
      }
      if (validationResult.error) {
        // Si la validación de los datos de la solicitud falla, entonces retorna error 422 / Invalid Request
        httpResponse = {
          data: null,
          errors: ['invalid_request'],
        }
        return res.status(422).send(httpResponse)
      } else {
        const tokenInformation: any = JsonWebToken.verify(data.token)
        if (!tokenInformation.username) {
          httpResponse = {
            data: null,
            errors: ['An error ocurred verifying the token'],
          }
          return res.status(401).send(httpResponse)
        }

        const response: any = await PutCryptoCurrencies.put(
          tokenInformation.username,
          data.cryptoCurrencies
        )
        if (response.errors && response.errors.length) {
          return res.status(400).send(response)
        }
        return res.status(200).send(response)
      }
    } catch (error) {
      // tslint:disable:no-console
      console.log(error)
      res.status(500).send({ error })
    }
  })
  app.get('/list-cryptocurrencies', async (req: any, res) => {
    try {
      // tslint:disable:no-console
      console.log(req.query)
      const validationResult = getCryptoCurrencies.validate(req.query)
      let httpResponse: Response = {
        data: null,
        errors: [],
      }
      if (validationResult.error) {
        // Si la validación de los datos de la solicitud falla, entonces retorna error 422 / Invalid Request
        httpResponse = {
          data: null,
          errors: ['invalid_request'],
        }
        return res.status(422).send(httpResponse)
      } else {
        const tokenInformation: any = JsonWebToken.verify(req.query.token)

        const response: any = await ListCryptoCurrencies.list(
          tokenInformation.username
        )
        if (response.errors && response.errors.length) {
          return res.status(400).send(response)
        }
        return res.status(200).send(response)
      }
    } catch (error) {
      return res.status(500).send({ error })
    }
  })
}

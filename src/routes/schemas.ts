import joi from 'joi'

export const signup = joi
  .object()
  .keys({
    name: joi.string().required(),
    lastName: joi.string().required(),
    username: joi.string().required(),
    password: joi.string().required(),
    favoriteCurrency: joi.string().valid('ars', 'eur', 'dol').required(),
  })
  .required()

export const login = joi
  .object()
  .keys({
    username: joi.string().required(),
    password: joi.string().required(),
  })
  .required()

export const getCryptoCurrencies = joi
  .object()
  .keys({
    token: joi.string().required(),
  })
  .required()

export const putCryptoCurrencies = joi
  .object()
  .keys({
    cryptoCurrencies: joi.array().items(
      joi.object().keys({
        currencyId: joi.string().required(),
        currencySymbol: joi.string().required(),
        currencyPrice: joi.number().required(),
        currencyName: joi.string().required(),
        currencyImage: joi.string().required(),
        currencyLastUpdated: joi.string().required(),
      })
    ),
    token: joi.string().required(),
  })
  .required()

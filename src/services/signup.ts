import { CommonsUsers } from '../commons/users'
import { SingUpRequest } from '../models/request-models/signUp.model'

export class SignUp {
  public static signup = async (body: SingUpRequest) => {
    try {
      const exists = await CommonsUsers.checkUserExists(body.username)
      if (exists) {
        return {
          data: {},
          errors: [`El usuario ${body.username} que intenta crear ya existe.`],
        }
      }

      const result = await CommonsUsers.createUser(body)

      if (!result) {
        return {
          data: {},
          errors: [`El usuario ${body.username} no fue creado.`],
        }
      }

      const response: any = {
        data: 'Usuario creado correctamente',
        errors: [],
      }
      return response
    } catch (error) {
      return {
        data: {},
        errors: [`An error occurred trying to create user ${error}`],
      }
    }
  }
}

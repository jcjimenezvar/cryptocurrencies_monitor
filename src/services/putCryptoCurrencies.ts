import { PutCryptoCurrenciesRequest } from '../models/request-models/putCryptoCurrencies'
import { CommonsUsers } from '../commons/users'
import { UsersVsCryptoCurrencies } from '../commons/usersVsCryptocurrencies'
import { Constants } from '../utils/constants'

export class PutCryptoCurrencies {
  public static put = async (
    username: string,
    currenciesData: PutCryptoCurrenciesRequest[]
  ) => {
    const user: any = await CommonsUsers.getUserByUsername(username)
    if (!user) {
      return {
        data: {},
        errors: [`El usuario ${username} no existe.`],
      }
    }

    if (
      await PutCryptoCurrencies.validateUserCryptocurrencies(
        user.username,
        currenciesData
      )
    ) {
      return {
        data: {},
        errors: [
          `Al menos una de las criptomonedas seleccionadas ya esta asociada al usuario ó ya tiene el limite permitido (25)`,
        ],
      }
    }

    await PutCryptoCurrencies.putUserCryptocurrencies(username, currenciesData)

    return {
      data: `Información creada correctamente.`,
    }
  }

  private static validateUserCryptocurrencies = async (
    username: string,
    currenciesData: PutCryptoCurrenciesRequest[]
  ) => {
    let alreadyHasCryptoCurrencies: boolean = false

    const ids = currenciesData.map(
      (data: PutCryptoCurrenciesRequest) => data.currencyId
    )

    const cryptoCurrenciesData: [] =
      await UsersVsCryptoCurrencies.getCryptoCurrenciesByUsernameAndIds(
        username,
        ids
      )

    if (
      cryptoCurrenciesData.length ||
      cryptoCurrenciesData.length > Constants.MAX_CRYPTO_CURRENCIES_BY_USER
    ) {
      alreadyHasCryptoCurrencies = true
    }

    return alreadyHasCryptoCurrencies
  }

  private static putUserCryptocurrencies = async (
    username: string,
    currenciesData: PutCryptoCurrenciesRequest[]
  ) => {
    await UsersVsCryptoCurrencies.putCryptoCurrenciesForUser(
      username,
      currenciesData
    )
  }
}

import { CommonsUsers } from '../commons/users'
import { LoginRequest } from '../models/request-models/login.model'
import { JsonWebToken } from '../commons/jsonWebToken'

export class Login {
  public static login = async (body: LoginRequest) => {
    if (!(await CommonsUsers.checkUserExists(body.username))) {
      return {
        data: {},
        errors: [`El usuario ${body.username} no existe.`],
      }
    }

    const user = await CommonsUsers.getUser(body)

    if (!user) {
      return {
        data: {},
        errors: [`Usuario o contraseña incorrectos`],
      }
    }

    const JWT = JsonWebToken.generate(body.username)

    return {
      data: JWT,
    }
  }
}

import axios from 'axios'
import { CommonsUsers } from '../commons/users'

export class GetCryptoCurrencies {
  public static get = async (username: string) => {
    const user: any = await CommonsUsers.getUserByUsername(username)

    if (!user) {
      return {
        data: {},
        errors: [`El usuario ${username} no existe.`],
      }
    }

    const response = await axios.get(
      'https://api.coingecko.com/api/v3/coins/markets',
      {
        headers: { accept: 'application/json' },
        params: {
          vs_currency: user.favorite_currency,
          order: 'market_cap_desc',
          per_page: 100,
          page: 1,
          sparkline: false,
        },
      }
    )

    if (!response && !response.data) {
      return {
        data: {},
        errors: [`No se encontro información de cryptocurrencies.`],
      }
    }

    const finalResponse: any[] = []

    response.data.forEach((currency: any) => {
      finalResponse.push({
        id: currency.id,
        symbol: currency.symbol,
        price: currency.current_price,
        name: currency.name,
        image: currency.image,
        lastUpdated: currency.last_updated,
      })
    })

    return finalResponse
  }
}

import { UsersVsCryptoCurrencies } from '../commons/usersVsCryptocurrencies'
import { CommonsUsers } from '../commons/users'
import axios from 'axios'
import { Constants } from '../utils/constants'
import { ListCryptoCurrenciesResponse } from '../models/request-models/listCryptocurrencies.model'

export class ListCryptoCurrencies {
  public static list = async (username: string) => {
    const user: any = await CommonsUsers.getUserByUsername(username)

    if (!user) {
      return {
        data: {},
        errors: [`El usuario ${username} no existe.`],
      }
    }

    const userCryptocurrencies: any[] =
      await UsersVsCryptoCurrencies.getCryptoCurrenciesByUsername(username)

    if (!userCryptocurrencies.length) {
      return {
        data: {},
        errors: [`El usuario ${username} no tiene cryptocurrencies`],
      }
    }

    const currencies: any = await ListCryptoCurrencies.getAllVsCurrencies()
    const response =
      await ListCryptoCurrencies.getAllCyrptocurrenciesInformation(
        userCryptocurrencies
          .map((currency: any) => currency.currency_id)
          .join(','),
        currencies
      )

    return {
      data: response,
    }
  }

  private static getAllCyrptocurrenciesInformation = async (
    ids: string,
    currencies: string[]
  ) => {
    const pricesByCurrency: ListCryptoCurrenciesResponse[] = []

    for (const currency of currencies) {
      const response = await axios.get(
        'https://api.coingecko.com/api/v3/coins/markets',
        {
          headers: { accept: 'application/json' },
          params: {
            vs_currency: currency,
            order: 'market_cap_desc',
            sparkline: false,
            ids,
          },
        }
      )

      if (!response && !response.data) {
        return {
          data: {},
          errors: [`No se encontró información.`],
        }
      } else {
        const data: any[] = response.data
        data.forEach((curr: any) => {
          pricesByCurrency.push({
            symbol: curr.symbol,
            currencyName: curr.name,
            currencyImage: curr.image,
            currency,
            price: curr.current_price,
            lastUpdateDate: curr.last_updated,
          })
        })
      }
    }
    return pricesByCurrency
  }

  private static getAllVsCurrencies = async () => {
    const response = await axios.get(
      'https://api.coingecko.com/api/v3/simple/supported_vs_currencies',
      {
        headers: { accept: 'application/json' },
      }
    )

    if (!response && !response.data) {
      return {
        data: {},
        errors: [
          `No se encontro información acerca de las monedas permitidas.`,
        ],
      }
    }

    const vsCurrenciesData: string[] = response.data

    const permitedCurrencies: string[] = vsCurrenciesData.filter(
      (currency: string) =>
        currency === Constants.ARS ||
        currency === Constants.EUR ||
        currency === Constants.USD
    )

    return permitedCurrencies
  }
}

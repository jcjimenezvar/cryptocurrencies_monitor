import { LoginRequest } from '../models/request-models/login.model'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'
dotenv.config()
export class JsonWebToken {
  public static generate = (username: string) => {
    const JWT = jwt.sign({ username }, process.env.JWTSECRET, {
      expiresIn: process.env.JWTTIME,
    })
    return JWT
  }
  public static verify = (token: string) => {
    try {
      const decoded = jwt.verify(token, process.env.JWTSECRET)
      return decoded
    } catch (error) {
      throw error
    }
  }
}

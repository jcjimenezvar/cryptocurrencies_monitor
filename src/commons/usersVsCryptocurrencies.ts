import { UsersVsCryptocurrencies } from '../models/db-models/userVsCryptocurrencies'
import { Op } from 'sequelize'
import { PutCryptoCurrenciesRequest } from '../models/request-models/putCryptoCurrencies'

export class UsersVsCryptoCurrencies {
  public static getCryptoCurrenciesByUsernameAndIds = async (
    username: string,
    cryptoCurrenciesIds: string[]
  ) => {
    const cryptoCurrencies: any = await UsersVsCryptocurrencies.findAll({
      where: {
        currency_id: {
          [Op.in]: cryptoCurrenciesIds,
        },
        username,
      },
      raw: true
    })

    return cryptoCurrencies
  }

  public static getCryptoCurrenciesByUsername = async (username: string) => {
    const cryptoCurrencies: any = await UsersVsCryptocurrencies.findAll({
      where: {
        username,
      },
      raw: true,
    })

    return cryptoCurrencies
  }

  public static putCryptoCurrenciesForUser = async (
    username: string,
    cryptoCurrenciesData: PutCryptoCurrenciesRequest[]
  ) => {
    for (const data of cryptoCurrenciesData) {
      await UsersVsCryptocurrencies.create({
        username,
        currency_id: data.currencyId,
        currency_symbol: data.currencySymbol,
        currency_price: data.currencyPrice,
        currency_name: data.currencyName,
        currency_image: data.currencyImage,
        currency_last_update: data.currencyLastUpdated,
      })
    }
  }
}

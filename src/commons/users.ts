import { LoginRequest } from '../models/request-models/login.model'
import { Users } from '../models/db-models/users'

export class CommonsUsers {
  public static checkUserExists = async (username: string) => {
    let userExists = false
    const user = await Users.findOne({
      where: {
        username,
      },
      raw: true,
    })

    if (user) {
      userExists = true
    }

    return userExists
  }

  public static createUser = async (data: any) => {
    let created = false
    const user = await Users.create(
      {
        user_name: data.name,
        user_last_name: data.lastName,
        username: data.username,
        user_password: data.password,
        favorite_currency: data.favoriteCurrency,
      },
      {
        returning: true,
        raw: true,
      }
    )

    if (user) created = true
    return created
  }

  public static getUser = async (data: LoginRequest) => {
    return await Users.findOne({
      where: {
        username: data.username,
        user_password: data.password,
      },
      raw: true,
    })
  }

  public static getUserByUsername = async (username: string) => {
    return await Users.findOne({
      where: {
        username,
      },
      raw: true,
    })
  }
}

export const Constants = {
  ARS: 'ars',
  USD: 'usd',
  EUR: 'eur',
  DEFAULT_PER_PAGE_VALUE: 100,
  MAX_CRYPTO_CURRENCIES_BY_USER: 25

}
